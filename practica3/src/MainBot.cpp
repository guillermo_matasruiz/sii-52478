#include "Bot.h"
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>

//Archivo que contiene el programa principal BOT

int main(){
	Bot bot;
	bot.openFichero();
	bot.proyectar();
	while(1){
		usleep(25000); //tiempo en microsegundos
		bot.actuador();
	}
	return 0;
}
