//////////////////////////////////////////////////////////////////////
//Archivo revisado por Guillermo Matas - 52478
//////////////////////////////////////////////////////////////////////
// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close (fd);
	unlink ("Mififo");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for (int i=0;i<esferas.size();i++)
		esferas[i].Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	for(int i=0;i<esferas.size();i++)
	{
		esferas[i].Mueve(0.025f);
	}
	for(int i=0;i<paredes.size();i++)
	{
		for(int j=0;j<esferas.size();j++)
			paredes[i].Rebota(esferas[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	for(int i=0;i<esferas.size();i++){
		jugador1.Rebota(esferas[i]);
		jugador2.Rebota(esferas[i]);
		if(fondo_izq.Rebota(esferas[i]))
		{
			esferas[i].radio=0.5f;
			esferas[i].centro.x=0;
			esferas[i].centro.y=rand()/(float)RAND_MAX;
			esferas[i].velocidad.x=-(i+1)*(2+2*rand()/(float)RAND_MAX);
			esferas[i].velocidad.y=-(i+1)*(2+2*rand()/(float)RAND_MAX);
			puntos2++;
			char mensaje2[100]="Jugador 2 marca 1 punto, lleva un total de ";
			char buffer2[10];
			sprintf(buffer2,"%d",puntos2);
			strcat(mensaje2,buffer2);
			strcat(mensaje2," puntos\n");
			write( fd, mensaje2 , strlen(mensaje2)*sizeof(char));
		}

		if(fondo_dcho.Rebota(esferas[i]))
		{
			esferas[i].radio=0.5f;
			esferas[i].centro.x=0;
			esferas[i].centro.y=rand()/(float)RAND_MAX;
			esferas[i].velocidad.x=-(i+1)*(2+2*rand()/(float)RAND_MAX);
			esferas[i].velocidad.y=-(i+1)*(2+2*rand()/(float)RAND_MAX);
			puntos1++;
			char mensaje[100]="Jugador 1 marca 1 punto, lleva un total de ";
			char buffer1[10];
			sprintf(buffer1,"%d",puntos1);
			strcat(mensaje,buffer1);
			strcat(mensaje," puntos\n");
			write( fd, mensaje , strlen(mensaje)*sizeof(char));
		}
	}
	if((puntos2>2*esferas.size() or puntos1>2*esferas.size()) and esferas.size()<3)
	{
		Esfera e;
		esferas.push_back(e);
	}
//Proyeccion en memoria
//Esfera
	mem->esfera.centro.y=esferas[0].centro.y;

//Raqueta

	mem->raqueta1.y1=jugador2.y1;
	mem->raqueta1.y2=jugador2.y2;
//Accion
	
	unsigned char tecla;
	if(mem->accion==-1){
		tecla='l';
	}
	else if(mem->accion==1){
		tecla='o';
	}
	else{ 
		tecla='c';	
	}
	CMundo::OnKeyboardDown(tecla, 0, 0);
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	
//Memoria Compartida
	void *p_void;
	float centro= (jugador2.y2+jugador2.y1)/2.0f;
	int acc=0;	
	struct stat bstat;
	fdmem= open("Memoria",O_RDWR|O_TRUNC|O_CREAT,0666);
	write (fdmem, &memoria,sizeof(mem));
	fstat(fdmem,&bstat);
	p_void=mmap(NULL,bstat.st_size,PROT_READ|PROT_WRITE,MAP_SHARED, fdmem, 0);
	close(fdmem);
	mem=(DatosMemCompartida *)p_void;	
//FIFO
	fd = open ( "Mififo", O_WRONLY);
//Esferas
	Esfera aux;
	esferas.push_back(aux);
//Paredes
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
